<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MonsterController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


// ROUTE HOME
Route::get('/',[HomeController::class,'index'])->name('home.index');
//  ROUTE INDEX des monstres
Route::get('/monsters',[MonsterController::class,'index'])->name('monsters.index');
// ROUTE SHOW des monstres
Route::get('/monsters/{monster}',[MonsterController::class,'show'])
->where(['monster' => '[1-9][0-9]*'])
->name('monsters.show');
// ROUTE INDEX des users
Route::get('/users',[UserController::class,'index'])->name('users.index');
// ROUTE SHOW des users
Route::get('/users/{user}',[UserController::class,'show'])
->where(['user' => '[1-9][0-9]*'])
->name('users.show');
