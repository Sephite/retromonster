<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Monster;
use App\Models\Comment;

class MonsterController extends Controller
{
    public function index(){
        $monsters = Monster::all()->take(9);
        return view('monsters.index', compact('monsters'));
    }

    public function show(int $id){
        $monster = Monster::find($id);

        return view('monsters.show', compact('monster'));
    }
}
