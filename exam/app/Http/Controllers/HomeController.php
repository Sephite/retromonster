<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Monster;


class HomeController extends Controller
{
    public function index(){
        $randMonster = Monster::inRandomOrder()->first();
        $lastMonsters = Monster::orderBy('created_at', 'DESC')->limit(3)->get();
        return view('home.index', compact('randMonster','lastMonsters'));
    }
}
