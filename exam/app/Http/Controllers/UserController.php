<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index(){
        $users = User::all()->take(9);
        return view('users.index', compact('users'));
    }

    public function show(int $id){
        $user = User::find($id);
        return view('users.show', compact('user'));
    }


}
