<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Monster extends Model
{
    use HasFactory;
    public function type(){
        return $this->belongsTo('App\Models\Monster_type');
    }
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function note(){
        return $this->hasMany('App\Models\Notation');
    }
    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }
}
