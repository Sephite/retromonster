@extends('template.index')


@section('content')

<h1 class="text-4xl">Les monstres qui on été créé par {{ $user->username }}</h1>


<section class="mb-20 py-8">

    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
@foreach ($user->monsters as $monster)

@php
$total = 0;
$nbrOfItems = count($monster->note);
@endphp
@foreach($monster->note as $item)
    @php
    $total += $item['notation'];
    @endphp
@endforeach
<article
        class="relative bg-gray-700 rounded-lg shadow-lg hover:shadow-2xl transition-shadow duration-300 monster-card"
        data-monster-type="spectral"
      >
        <img
          class="w-full h-48 object-cover rounded-t-lg"
          src="{{ asset('img/' . $monster->image_url) }}"
          alt="{{ $monster->name }}"
        />
        <div class="p-4">
          <h3 class="text-xl font-bold">{{ $monster->name }}</h3>
          <h4 class="mb-2">
            <a href="#" class="text-red-400 hover:underline"
              >{{ $monster->user->username }}</a
            >
          </h4>
          <p class="text-gray-300 text-sm mb-2">
            {{ $monster->description }}
          </p>
          <div class="flex justify-between items-center mb-2">
            <div>
                @if($nbrOfItems > 0)
                <span class="text-yellow-400">
                    @php
                        $averageRating = $total / $nbrOfItems;
                        $fullStars = floor($averageRating);
                        $halfStar = ceil($averageRating) - $fullStars;
                        $emptyStars = 5 - $fullStars - $halfStar;
                    @endphp

                    @for ($i = 0; $i < $fullStars; $i++)
                        ★
                    @endfor

                    @if ($halfStar > 0)
                        ☆
                    @endif

                    @for ($i = 0; $i < $emptyStars; $i++)
                        ☆
                    @endfor
                </span>

                <span class="text-gray-300 text-sm">
                    {{ number_format($total / $nbrOfItems, 2) }}/5
                </span>
            @else
                <span class="text-gray-300 text-sm">
                    Aucune note disponible.
                </span>
            @endif
              </span>
            </div>
            <span class="text-sm text-gray-300">{{ $monster->type->name }}</span>
          </div>
          <div class="flex justify-between items-center mb-4">
            <span class="text-sm text-gray-300">PV: {{ $monster->pv }}</span>
            <span class="text-sm text-gray-300">Attaque: {{ $monster->attack }}</span>
          </div>
          <div class="text-center">
            <a
            href="{{ route('monsters.show', ['monster' => $monster->id]) }}"
              class="inline-block text-white bg-red-500 hover:bg-red-700 rounded-full px-4 py-2 transition-colors duration-300"
              >Plus de détails</a
            >
          </div>
        </div>
        <div class="absolute top-4 right-4">
          <button
            class="text-white bg-gray-400 hover:bg-red-700 rounded-full p-2 transition-colors duration-300"
            style="
              width: 40px;
              height: 40px;
              display: flex;
              justify-content: center;
              align-items: center;
            "
          >
            <i class="fa fa-bookmark"></i>
          </button>
        </div>
      </article>
@endforeach
</div>
</section>
@endsection
