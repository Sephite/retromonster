@extends('template.index')

{{-- j'ai $users --}}
@section('content')
<div class="grid grid-cols-4 gap-4">
@foreach ($users as $user)
<div class='bg-blue-500  py-8 px-4 flex flex-col gap-4 text-center'>
    <h2 class="text-2xl ">{{ $user->username }}</h2>

        <a href="{{ route('users.show', ['user' => $user->id]) }}" class="bg-red-500 text-center text-xl">Voir plus</a>


</div>


@endforeach
</div>
@endsection
