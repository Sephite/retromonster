@extends('template.index')
@php
$totalNotes = 0;
$numberOfItems = count($randMonster->note);
@endphp
@foreach($randMonster->note as $item)
    @php
    $totalNotes += $item['notation'];
    @endphp
@endforeach

@section('content')

<section class="mb-20">
    <div
      class="bg-gray-700 rounded-lg shadow-lg monster-card"
      data-monster-type="aquatique"
    >
      <div class="md:flex">
        <!-- Image du monstre -->
        <div class="w-full md:w-1/2 relative">
          <img
            class="w-full h-full object-cover rounded-t-lg md:rounded-l-lg md:rounded-t-none"
            src="{{ asset('img/' . $randMonster->image_url) }}"
            alt="{{ $randMonster->name }}"
          />
          <div class="absolute top-4 right-4">
            <button
              class="text-white bg-gray-400 hover:bg-red-700 rounded-full p-2 transition-colors duration-300"
              style="
                width: 40px;
                height: 40px;
                display: flex;
                justify-content: center;
                align-items: center;
              "
            >
              <i class="fa fa-bookmark"></i>
            </button>
          </div>
        </div>

        <!-- Détails du monstre -->
        <div class="p-6 md:w-1/2">
          <h2 class="text-3xl font-bold mb-2 creepster">
            {{ $randMonster->name }}
          </h2>
          <p class="text-gray-300 text-sm mb-4">
            {{ $randMonster->description }}

          </p>
          <div class="mb-4">
            <strong class="text-white">Créateur:</strong>
            <span class="text-red-400">{{ $randMonster->user->username }}</span>
          </div>
          <div class="mb-4">
            <div>
              <strong class="text-white">Type:</strong>
              <span class="text-gray-300">{{ $randMonster->type->name }}</span>
            </div>
            <div>
              <strong class="text-white">PV:</strong>
              <span class="text-gray-300">{{ $randMonster->pv }}</span>
            </div>
            <div>
              <strong class="text-white">Attaque:</strong>
              <span class="text-gray-300">{{ $randMonster->attack }}</span>
            </div>
            <div>
              <strong class="text-white">Défense:</strong>
              <span class="text-gray-300">{{ $randMonster->defense }}</span>
            </div>
          </div>
          <div class="mb-4">



            @if($numberOfItems > 0)
            <span class="text-yellow-400">
                @php
                    $avgRating = $totalNotes / $numberOfItems;
                    $fullStars = floor($avgRating);
                    $halfStar = ceil($avgRating) - $fullStars;
                    $emptyStars = 5 - $fullStars - $halfStar;
                @endphp

                @for ($i = 0; $i < $fullStars; $i++)
                    ★
                @endfor

                @if ($halfStar > 0)
                    ☆
                @endif

                @for ($i = 0; $i < $emptyStars; $i++)
                    ☆
                @endfor
            </span>

            <span class="text-gray-300 text-sm">
                {{ number_format($avgRating, 2) }}/5
            </span>
        @else
            <span class="text-gray-300 text-sm">
                Aucune note disponible.
            </span>
        @endif
          </div>
          <div class="">
            <a
              href="{{ route('monsters.show', ['monster' => $randMonster->id]) }}"
              class="inline-block text-white bg-red-500 hover:bg-red-700 rounded-full px-4 py-2 transition-colors duration-300"
              >Plus de détails</a
            >
          </div>
        </div>
      </div>
    </div>
  </section>


<section class="mb-20">
    <h2 class="text-2xl font-bold mb-4 creepster">
      Derniers monstres ajoutés
    </h2>
    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
        @foreach ($lastMonsters as $monster)

                @php
        $total = 0;
        $nbrOfItems = count($monster->note);
        @endphp
        @foreach($monster->note as $item)
            @php
            $total += $item['notation'];
            @endphp
        @endforeach
        <article
                class="relative bg-gray-700 rounded-lg shadow-lg hover:shadow-2xl transition-shadow duration-300 monster-card"
                data-monster-type="spectral"
              >
                <img
                  class="w-full h-48 object-cover rounded-t-lg"
                  src="{{ asset('img/' . $monster->image_url) }}"
                  alt="{{ $monster->name }}"
                />
                <div class="p-4">
                  <h3 class="text-xl font-bold">{{ $monster->name }}</h3>
                  <h4 class="mb-2">
                    <a href="#" class="text-red-400 hover:underline"
                      >{{ $monster->user->username }}</a
                    >
                  </h4>
                  <p class="text-gray-300 text-sm mb-2">
                    {{ $monster->description }}
                  </p>
                  <div class="flex justify-between items-center mb-2">
                    <div>
                        @if($nbrOfItems > 0)
                        <span class="text-yellow-400">
                            @php
                                $averageRating = $total / $nbrOfItems;
                                $fullStars = floor($averageRating);
                                $halfStar = ceil($averageRating) - $fullStars;
                                $emptyStars = 5 - $fullStars - $halfStar;
                            @endphp

                            @for ($i = 0; $i < $fullStars; $i++)
                                ★
                            @endfor

                            @if ($halfStar > 0)
                                ☆
                            @endif

                            @for ($i = 0; $i < $emptyStars; $i++)
                                ☆
                            @endfor
                        </span>

                        <span class="text-gray-300 text-sm">
                            {{ number_format($total / $nbrOfItems, 2) }}/5
                        </span>
                    @else
                        <span class="text-gray-300 text-sm">
                            Aucune note disponible.
                        </span>
                    @endif
                      </span>
                    </div>
                    <span class="text-sm text-gray-300">{{ $monster->type->name }}</span>
                  </div>
                  <div class="flex justify-between items-center mb-4">
                    <span class="text-sm text-gray-300">PV: {{ $monster->pv }}</span>
                    <span class="text-sm text-gray-300">Attaque: {{ $monster->attack }}</span>
                  </div>
                  <div class="text-center">
                    <a
                    href="{{ route('monsters.show', ['monster' => $monster->id]) }}"
                      class="inline-block text-white bg-red-500 hover:bg-red-700 rounded-full px-4 py-2 transition-colors duration-300"
                      >Plus de détails</a
                    >
                  </div>
                </div>
                <div class="absolute top-4 right-4">
                  <button
                    class="text-white bg-gray-400 hover:bg-red-700 rounded-full p-2 transition-colors duration-300"
                    style="
                      width: 40px;
                      height: 40px;
                      display: flex;
                      justify-content: center;
                      align-items: center;
                    "
                  >
                    <i class="fa fa-bookmark"></i>
                  </button>
                </div>
              </article>
        @endforeach
    </div>
  </section>
@endsection
